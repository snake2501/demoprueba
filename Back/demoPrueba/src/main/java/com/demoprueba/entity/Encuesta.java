package com.demoprueba.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Carlos Ferro
 */
@Entity
@Access(AccessType.FIELD)
@Data
@NoArgsConstructor //JPA
@AllArgsConstructor
public class Encuesta implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Integer id_usuario;

    @Column
    private Integer ndocumento; 

    @Column
    private String email;

    @Column
    private String comentario;

    @Column
    private String marcafavorita;

    @Column
    private Date fecha;

}
