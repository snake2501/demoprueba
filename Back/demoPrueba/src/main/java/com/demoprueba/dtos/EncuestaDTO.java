package com.demoprueba.dtos;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Carlos Ferro
 */
@Data
public class EncuestaDTO implements Serializable {

    private Long id;
    private Integer id_usuario;
    private Integer ndocumento;
    private String email;
    private String comentario;
    private String marcafavorita;
    private Date fecha;

}
