package com.demoprueba.dtos;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Carlos Ferro
 */
@Data
public class UsuarioDTO implements Serializable {

    private Long id;
    private String login;
    private String password;
    private String nombre;
    private String rol;
    private Integer intentos;
    private Date ultimoingreso;

}
