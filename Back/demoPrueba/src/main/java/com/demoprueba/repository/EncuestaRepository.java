package com.demoprueba.repository;

import com.demoprueba.entity.Encuesta;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
/**
 *
 * @author CMFerroV
 */
@Repository
public interface EncuestaRepository extends JpaRepository<Encuesta, Long>{

    @Query("select obj from Encuesta obj")
    public Page<Encuesta> findAll(Pageable pageable);

	@Query("select obj from Encuesta obj where obj.id =:id")
    public Optional<Encuesta> findById(@Param("id")Long id);

	@Query("select obj from Encuesta obj where obj.id_usuario =:id_usuario")
    public Optional<Encuesta> findById_usuario(@Param("id_usuario")Integer id_usuario);

	@Query("select obj from Encuesta obj where obj.ndocumento =:ndocumento")
    public Optional<Encuesta> findByNdocumento(@Param("ndocumento")Integer ndocumento);

	@Query("select obj from Encuesta obj where obj.email =:email")
    public Optional<Encuesta> findByEmail(@Param("email")String email);

	@Query("select obj from Encuesta obj where obj.comentario =:comentario")
    public Optional<Encuesta> findByComentario(@Param("comentario")String comentario);

	@Query("select obj from Encuesta obj where obj.marcafavorita =:marcafavorita")
    public Optional<Encuesta> findByMarcafavorita(@Param("marcafavorita")String marcafavorita);

	@Query("select obj from Encuesta obj where obj.fecha =:fecha")
    public Optional<Encuesta> findByFecha(@Param("fecha")Date fecha);


    
}

