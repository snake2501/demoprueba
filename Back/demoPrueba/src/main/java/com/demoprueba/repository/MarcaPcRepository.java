package com.demoprueba.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.demoprueba.entity.MarcaPc;

/**
 *
 * @author CMFerroV
 */
@Repository
public interface MarcaPcRepository extends JpaRepository<MarcaPc, Long> {

    @Query("select obj from MarcaPc obj")
    public List<MarcaPc> findAll();


}
