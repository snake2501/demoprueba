package com.demoprueba.controllers;

import com.demoprueba.dtos.UsuarioDTO;
import java.util.List;
import com.demoprueba.service.UsuarioService;
import java.util.Date;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.demoprueba.entity.*;
import com.demoprueba.service.MarcaPcService;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import static org.hibernate.internal.util.StringHelper.isBlank;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author CMFerroV
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController("marcaPc")
public class MarcaPcRest {

    @Autowired
    private MarcaPcService marcaPcService;

    @ApiOperation(value = "")
    @GetMapping("marcaPc/all")
    public List<MarcaPc> allUsuario() {
        return marcaPcService.allMarcaPc();
    }

}
