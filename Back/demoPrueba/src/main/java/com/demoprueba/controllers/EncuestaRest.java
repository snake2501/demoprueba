package com.demoprueba.controllers;

import com.demoprueba.dtos.EncuestaDTO;
import com.demoprueba.service.EncuestaService;
import java.util.ArrayList;
import java.util.List;

import java.util.Date;
import java.util.Optional;
import static org.hibernate.internal.util.StringHelper.isBlank;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.demoprueba.entity.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 *
 * @author CMFerroV
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController("encuesta")
public class EncuestaRest {

    @Autowired
    private EncuestaService encuestaService;

    @Autowired
    private ModelMapper modelMapper;

    private EncuestaDTO convertToDto(Encuesta post) {
        EncuestaDTO postDto = modelMapper.map(post, EncuestaDTO.class);
        return postDto;
    }

    private EncuestaDTO convertToDto(Optional<Encuesta> postOpt) {
        Encuesta post = null;
        if (postOpt.isPresent()) {
            post = postOpt.get();
        } else {
            return null;
        }
        return convertToDto(post);
    }

    private Page<EncuestaDTO> convertToDto(Page<Encuesta> post, Pageable pageable) {
        List<EncuestaDTO> postDto = new ArrayList<>();

        for (Encuesta entity : post.getContent()) {
            postDto.add(convertToDto(entity));
        }

        return new PageImpl<>(postDto, pageable, post.getTotalElements());
    }

    private Encuesta convertToEntity(EncuestaDTO postDto) {
        Encuesta post = modelMapper.map(postDto, Encuesta.class);
        return post;
    }

    @ApiOperation(value = "")
    @PostMapping(path = "encuesta/saveorupdate")
    public EncuestaDTO saveOrUpdateEncuestas(@RequestBody EncuestaDTO Encuestas) {
        return convertToDto(encuestaService.saveOrUpdateEncuesta(convertToEntity(Encuestas)));
    }

    @ApiOperation(value = "")
    @PostMapping(path = "encuesta/delete")
    public Boolean deleteEncuestas(@RequestBody Long id) {
        try {
            encuestaService.deleteEncuesta(encuestaService.encuestaById(id).get());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "")
    @GetMapping("encuesta/all")
    public Page<EncuestaDTO> allEncuesta(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer pagina,
            @RequestParam(name = "size", required = false, defaultValue = "10") Integer size) {
        Sort sort = isBlank("") ? Sort.unsorted() : Sort.by(1 == 1 ? Sort.Direction.ASC : Sort.Direction.DESC);
        Pageable pageable = PageRequest.of(pagina, size, sort);
        return convertToDto(encuestaService.allEncuesta(pageable), pageable);
    }

    @ApiOperation(value = "")
    @GetMapping("encuesta/ById/{id}")
    public EncuestaDTO encuestasById(@PathVariable Long id) {
        return convertToDto(encuestaService.encuestaById(id).get());
    }

    @ApiOperation(value = "")
    @GetMapping("encuesta/ById_usuario/{id_usuario}")
    public EncuestaDTO encuestasById_usuario(@PathVariable Integer id_usuario) {
        return convertToDto(encuestaService.encuestaById_usuario(id_usuario).get());
    }

    @ApiOperation(value = "")
    @GetMapping("encuesta/ByNdocumento/{ndocumento}")
    public EncuestaDTO encuestasByNdocumento(@PathVariable Integer ndocumento) {
        return convertToDto(encuestaService.encuestaByNdocumento(ndocumento).get());
    }

    @ApiOperation(value = "")
    @GetMapping("encuesta/ByEmail/{email}")
    public EncuestaDTO encuestasByEmail(@PathVariable String email) {
        return convertToDto(encuestaService.encuestaByEmail(email).get());
    }

    @ApiOperation(value = "")
    @GetMapping("encuesta/ByComentario/{comentario}")
    public EncuestaDTO encuestasByComentario(@PathVariable String comentario) {
        return convertToDto(encuestaService.encuestaByComentario(comentario).get());
    }

    @ApiOperation(value = "")
    @GetMapping("encuesta/ByMarcafavorita/{marcafavorita}")
    public EncuestaDTO encuestasByMarcafavorita(@PathVariable String marcafavorita) {
        return convertToDto(encuestaService.encuestaByMarcafavorita(marcafavorita).get());
    }

    @ApiOperation(value = "")
    @GetMapping("encuesta/ByFecha/{fecha}")
    public EncuestaDTO encuestasByFecha(@PathVariable Date fecha) {
        return convertToDto(encuestaService.encuestaByFecha(fecha).get());
    }

}
