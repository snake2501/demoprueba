package com.demoprueba.service;

import com.demoprueba.repository.EncuestaRepository;
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.demoprueba.entity.*;

/**
 *
 * @author CMFerroV
 */
@Service
public class EncuestaService {

	@Autowired
    private EncuestaRepository  encuestaRepository;

    public Encuesta saveOrUpdateEncuesta(Encuesta encuesta) {
        return  encuestaRepository.save( encuesta);
    }

    public Boolean deleteEncuesta(Encuesta encuesta) {
        try {
             encuestaRepository.delete(encuesta);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Page<Encuesta> allEncuesta(Pageable pageable) {
        return  encuestaRepository.findAll(pageable);
    }
	
   
    public Optional<Encuesta> encuestaById(Long id) {
        return encuestaRepository.findById(id);
    }	

    public Optional<Encuesta> encuestaById_usuario(Integer id_usuario) {
        return encuestaRepository.findById_usuario(id_usuario);
    }	

    public Optional<Encuesta> encuestaByNdocumento(Integer ndocumento) {
        return encuestaRepository.findByNdocumento(ndocumento);
    }	

    public Optional<Encuesta> encuestaByEmail(String email) {
        return encuestaRepository.findByEmail(email);
    }	

    public Optional<Encuesta> encuestaByComentario(String comentario) {
        return encuestaRepository.findByComentario(comentario);
    }	

    public Optional<Encuesta> encuestaByMarcafavorita(String marcafavorita) {
        return encuestaRepository.findByMarcafavorita(marcafavorita);
    }	

    public Optional<Encuesta> encuestaByFecha(Date fecha) {
        return encuestaRepository.findByFecha(fecha);
    }	


    
}

