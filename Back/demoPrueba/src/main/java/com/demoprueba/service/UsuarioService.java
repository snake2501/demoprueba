package com.demoprueba.service;

import com.demoprueba.dtos.UsuarioDTO;
import com.demoprueba.repository.UsuarioRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.demoprueba.entity.*;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageImpl;

/**
 *
 * @author CMFerroV
 */
@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private ModelMapper modelMapper;

    public UsuarioDTO convertToDto(Usuario post) {
        UsuarioDTO postDto = modelMapper.map(post, UsuarioDTO.class);
        return postDto;
    }

    public UsuarioDTO convertToDto(Optional<Usuario> postOpt) {
        Usuario post = null;
        if (postOpt.isPresent()) {
            post = postOpt.get();
        } else {
            return null;
        }
        return convertToDto(post);
    }

    public Page<UsuarioDTO> convertToDto(Page<Usuario> post, Pageable pageable) {
        List<UsuarioDTO> postDto = new ArrayList<>();

        for (Usuario entity : post.getContent()) {
            postDto.add(convertToDto(entity));
        }

        return new PageImpl<>(postDto, pageable, post.getTotalElements());
    }

    public Usuario convertToEntity(UsuarioDTO postDto) {
        Usuario post = modelMapper.map(postDto, Usuario.class);
        return post;
    }

    public Usuario saveOrUpdateUsuario(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    public Boolean deleteUsuario(Usuario usuario) {
        try {
            usuarioRepository.delete(usuario);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Page<Usuario> allUsuario(Pageable pageable) {
        return usuarioRepository.findAll(pageable);
    }

    public Optional<Usuario> usuarioById(Long id) {
        return usuarioRepository.findById(id);
    }

    public Optional<Usuario> usuarioByLoginPassword(String login, String password) {
        return usuarioRepository.findByLoginAndPassword(login,password);
    }

    public Optional<Usuario> usuarioByLogin(String login) {
        return usuarioRepository.findByLogin(login);
    }

    public Optional<Usuario> usuarioByPassword(String password) {
        return usuarioRepository.findByPassword(password);
    }

}
