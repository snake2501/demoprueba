package com.demoprueba.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demoprueba.entity.*;
import com.demoprueba.repository.MarcaPcRepository;
import java.util.List;
import org.modelmapper.ModelMapper;

/**
 *
 * @author CMFerroV
 */
@Service
public class MarcaPcService {

    @Autowired
    private MarcaPcRepository marcaPcRepository;
    
    @Autowired
    private ModelMapper modelMapper;

    public MarcaPc saveOrUpdateMarcaPc(MarcaPc usuario) {
        return marcaPcRepository.save(usuario);
    }

    public Boolean deleteMarcaPc(MarcaPc usuario) {
        try {
            marcaPcRepository.delete(usuario);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public List<MarcaPc> allMarcaPc() {
        return marcaPcRepository.findAll();
    }


}
