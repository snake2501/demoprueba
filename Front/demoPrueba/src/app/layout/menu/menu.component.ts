import { Component, DoCheck } from '@angular/core';
import { UtilService } from 'src/app/core/services/util.service';
import { AutenticacionService } from 'src/app/core/services/autenticacion.service';
import { Menu } from 'src/app/core/model/menu.model';

@Component({ 
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements DoCheck {


  menu: Array<Menu>;


  constructor(public _util: UtilService, public _aut: AutenticacionService) { }


  ngDoCheck(): void {
    this.menu = this._aut.menu;
  }
  

  get ocultarMenuRol(): boolean {
    return this._util.ocultarMenuRol;
  }


}

