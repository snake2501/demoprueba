import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { UtilService } from 'src/app/core/services/util.service';
import { Router } from '@angular/router';
import { AutenticacionService } from 'src/app/core/services/autenticacion.service';

@Component({
  selector: 'app-encabezado',
  templateUrl: './encabezado.component.html',
  styleUrls: ['./encabezado.component.scss']
})
export class EncabezadoComponent implements OnInit {


  public menuUsuario: MenuItem[];


  constructor(private router: Router, public _util: UtilService, public _aut: AutenticacionService) { }


  ngOnInit() {
    this.menuUsuario = [
      /*{
        label: 'Perfil',
        items: [
          {
            label: 'Info. usuario',
            icon: 'fa fa-address-book'
          },
          { label: 'Cambiar contraseña' }
        ]
      },*/
      {
        label: 'Cerrar sesión',
        icon: 'fa fa-address-book',
        items: [
          { label: 'Salir', icon: 'fa fa-address-book', command: (event: any) => { this.salir() } }
        ] 
      }
    ];
  }


  salir(){
    this._aut.cerrarSesion();
    this.router.navigate(["/login"]);
  }


}

