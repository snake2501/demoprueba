import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AutenticacionService } from './autenticacion.service';
import { Usuario } from '../model/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionGuardService implements CanActivate {

  constructor(private _aut: AutenticacionService, private router: Router) { }


  async canActivate(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): Promise<any> {
    if (this._aut._usuarioConectado !== undefined) {
      return true;
    } else {
      this.router.navigate(["/inicio"]);
    }

    return true;

  }

}
