import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LazyLoadEvent, SelectItem, MessageService } from 'primeng/api';

import { UsuarioDTO } from './usuario.dto';
import { Page } from 'src/app/common/util';
import { HttpParams, HttpResponse } from '@angular/common/http';
import { usuarioService } from 'src/app/core/services/usuario.service';
import { AutenticacionService } from 'src/app/core/services/autenticacion.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-usuario-List',
  templateUrl: './usuario-List.component.html',
  providers: [MessageService]
})
export class UsuarioListComponent implements OnInit {

  form: FormGroup;

  nav: any = {};


  id: number;
  login: string;
  password: string;
  filtro: string;
  nombre: string;
  
  usuarios: UsuarioDTO[];
  usuarioPage: Page<UsuarioDTO>;
  totalRecords: number;
  cols: any[];
  loading: boolean;
  pagitatorTable: boolean;
  rowsTable: number;
  sortBy: string;
  sortOrderBy: string;

  displayDialog: boolean;
  usuario: UsuarioDTO = {};
  selectedUsuario: UsuarioDTO;
  newusuario: boolean;
  eventoTabla: LazyLoadEvent;

  constructor(private fb: FormBuilder, private messageService: MessageService, private _usuarioService: usuarioService,
    public _aut: AutenticacionService, private router: Router) {

    this.nav = { back: '', next: '' };



    this.cols = [
      //{ field: 'id', header: 'id' },
      { field: 'nombre', header: 'nombre' },
      { field: 'login', header: 'login ' },
      { field: 'password', header: 'password' },

    ];

  }

  ngOnInit() {
    this.loading = true;
    this.pagitatorTable = true;
    this.rowsTable = 10;

    this.form = this.fb.group({
      id: new FormControl(''),
      login: new FormControl(''),
      password: new FormControl(''),
      nombre: new FormControl('')
    });

    this.id = null;
    this.login = null;
    this.password = null;
    this.nombre = null;
  }

  resetPage() {
    this.ngOnInit();
    this.loadUsuarioLazy(this.eventoTabla);
  }

  filtrar() {
    this.loadUsuarioLazy(this.eventoTabla);
  }

  onSubmit(value: string) {
    console.log("submit ok", this.form);
  }

  loadUsuarioLazy(event: LazyLoadEvent) {
    this.loading = true;
    this.eventoTabla = event;
    this.sortBy = event.sortField;
    this.sortOrderBy = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.filtro = this.filtro === '' ? undefined : this.filtro;

    this._usuarioService.all(event.first / event.rows, event.rows, this.filtro, this.sortBy, this.sortOrderBy).subscribe((res: HttpResponse<Object>) => {
      let page: any = res;
      console.log("result", res)
      this.usuarioPage = page.body;
      
      let respuesta = this.usuarioPage.content;
      this.totalRecords = this.usuarioPage.totalElements;
      this.usuarios = respuesta;
      console.log("result", this.usuarios)
      this.loading = false;
    });

  }

  showDialogToAdd() {
    this.newusuario = true;
    this.usuario = {};
    this.displayDialog = true;
  }

  castObjectInterface() {
    this.selectedUsuario = {};

    this.selectedUsuario.id = this.id;
    this.selectedUsuario.login = this.login;
    this.selectedUsuario.password = this.password;
    this.selectedUsuario.nombre = this.nombre;
  }

  save() {
    this.loading = true;
    this.castObjectInterface();
    this._usuarioService.addUsuario(this.selectedUsuario).subscribe((result) => {
      this.resetPage();
      this.loading = false;
    }, (err) => {
      console.log(err);
      this.loading = false;
    });
  }

  clean() {
    this.resetPage();
  }

  delete() {
    let index = this.usuarios.indexOf(this.selectedUsuario);
    this.usuarios = this.usuarios.filter((val, i) => i != index);
    this.usuario = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    //this.loadSelect(event.data.id);
  }

  loadSelect(id) {
    this._usuarioService.obtenerUsuarioPorid(id).subscribe((res: HttpResponse<Object>) => {
      let respuesta: any = res;
      console.log("byId");
      console.log(respuesta);
      if (respuesta != null) {

        this.id = respuesta.id;
        this.login = respuesta.login;
        this.password = respuesta.password;
        this.nombre = respuesta.nombre;
      }
      this.loading = false;
    });
  }

  cloneusuario(c: UsuarioDTO): UsuarioDTO {
    let usuario = {};
    for (let prop in c) {
      usuario[prop] = c[prop];
    }
    return usuario;
  }



}






